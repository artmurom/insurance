package com.osagoandkasko.bestonline;

import android.app.Application;

import com.osagoandkasko.bestonline.repository.RepositoryProvider;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;


public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        YandexMetricaConfig.Builder configBuilder = YandexMetricaConfig.newConfigBuilder(BuildConfig.APPMETRICA_KEY);
        YandexMetrica.activate(getApplicationContext(), configBuilder.build());
        YandexMetrica.enableActivityAutoTracking(this);

        RepositoryProvider.init(this);

    }

}


