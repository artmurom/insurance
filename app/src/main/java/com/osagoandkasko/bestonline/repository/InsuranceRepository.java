package com.osagoandkasko.bestonline.repository;

import com.osagoandkasko.bestonline.content.Data;
import com.osagoandkasko.bestonline.content.Insurance;
import com.osagoandkasko.bestonline.content.UrlItem;

import java.util.List;

public interface InsuranceRepository {

    List<Data> getData();

    List<UrlItem> getAllUrl(String title, String category);

}
