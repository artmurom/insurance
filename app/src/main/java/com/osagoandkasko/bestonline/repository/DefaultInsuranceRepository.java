package com.osagoandkasko.bestonline.repository;


import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;

import com.osagoandkasko.bestonline.R;
import com.osagoandkasko.bestonline.content.Category;
import com.osagoandkasko.bestonline.content.Data;
import com.osagoandkasko.bestonline.content.Insurance;
import com.osagoandkasko.bestonline.content.UrlItem;
import com.osagoandkasko.bestonline.db.DataBaseHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DefaultInsuranceRepository implements InsuranceRepository {

    private static final String TABLE_INSURANCE_NAME = "insurance";
    private static final String IMAGE_COLUMN = "image";

    private static final String PROMO_COLUMN = "promo";
    private static final String REFER_COLUMN = "refer";
    private static final String DESCRIPTION_FULL_ID_COLUMN = "description_full_id";
    private static final String CATEGORY_ID_COLUMN = "category_id";

    private static final String TABLE_CATEGORY_NAME = "category";

    private static final String ID_COLUMN = "_id";
    private static final String TITLE_COLUMN = "title";
    private static final String DESC1_COLUMN = "desc1";
    private static final String DESC2_COLUMN = "desc2";

    private static final String TABLE_DESCRIPTION_FULL = "description_full";
    private static final String DESC_FULL_COLUMN = "desc_full";


    private final String mUrl;

    private DataBaseHelper mDataBaseHelper;

    public DefaultInsuranceRepository(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        dataBaseHelper.upgradeDB();
        mDataBaseHelper = dataBaseHelper;

        mUrl = context.getString(R.string.base_url);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public List<Data> getData() {
        try {
            mDataBaseHelper.openDataBase();
            String queryCategory = String.format("SELECT * FROM %s", TABLE_CATEGORY_NAME);

            Cursor cursorCategory = mDataBaseHelper.cursorBack(queryCategory);

            List<Data> dataList = new ArrayList<>();

            List<Category> categoryList = new ArrayList<>();

            while (cursorCategory.moveToNext()) {
                int id = cursorCategory.getInt(cursorCategory.getColumnIndex(ID_COLUMN));
                String title = cursorCategory.getString(cursorCategory.getColumnIndex(TITLE_COLUMN));
                String desc1 = cursorCategory.getString(cursorCategory.getColumnIndex(DESC1_COLUMN));
                String desc2 = cursorCategory.getString(cursorCategory.getColumnIndex(DESC2_COLUMN));

                Category category = new Category(id, title, desc1, desc2);

                categoryList.add(category);
            }

            cursorCategory.close();

            for (Category category : categoryList) {

                String queryInsurance = String.format("SELECT * FROM %s i LEFT JOIN %s d ON i.[%s] = d.[%s] WHERE %s = %d", TABLE_INSURANCE_NAME, TABLE_DESCRIPTION_FULL, DESCRIPTION_FULL_ID_COLUMN, ID_COLUMN, CATEGORY_ID_COLUMN, category.getId());

                Cursor cursorInsurance = mDataBaseHelper.cursorBack(queryInsurance);

                List<Insurance> insuranceList = new ArrayList<>();

                while (cursorInsurance.moveToNext()) {
                    String title = cursorInsurance.getString(cursorInsurance.getColumnIndex(TITLE_COLUMN));
                    String image = cursorInsurance.getString(cursorInsurance.getColumnIndex(IMAGE_COLUMN));
                    String desc1 = cursorInsurance.getString(cursorInsurance.getColumnIndex(DESC1_COLUMN));
                    String desc2 = cursorInsurance.getString(cursorInsurance.getColumnIndex(DESC2_COLUMN));
                    String descFull = cursorInsurance.getString(cursorInsurance.getColumnIndex(DESC_FULL_COLUMN));
                    String refer = String.format(mUrl, cursorInsurance.getString(cursorInsurance.getColumnIndex(REFER_COLUMN)));
                    String promo = cursorInsurance.getString(cursorInsurance.getColumnIndex(PROMO_COLUMN));

                    Insurance insurance = new Insurance(title, image, desc1, desc2, descFull, refer, promo, category.getTitle());

                    insuranceList.add(insurance);
                }

                Data data = new Data(insuranceList, category);

                dataList.add(data);

                cursorInsurance.close();
            }


            mDataBaseHelper.closeDB();
            return dataList;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public List<UrlItem> getAllUrl(String title, String category) {
        try {
            mDataBaseHelper.openDataBase();

            String queryCategoryId = String.format("SELECT %s FROM %s WHERE %s = '%s'", ID_COLUMN, TABLE_CATEGORY_NAME, TITLE_COLUMN, category);

            Cursor cursorCategoryId = mDataBaseHelper.cursorBack(queryCategoryId);
            cursorCategoryId.moveToNext();
            int categoryId = cursorCategoryId.getInt(cursorCategoryId.getColumnIndex(ID_COLUMN));

            cursorCategoryId.close();


            String queryRefer = String.format("SELECT %s, %s FROM %s WHERE %s = '%s' AND %s = '%s'", DESC1_COLUMN, REFER_COLUMN, TABLE_INSURANCE_NAME, TITLE_COLUMN, title, CATEGORY_ID_COLUMN, categoryId);

            Cursor cursorRefer = mDataBaseHelper.cursorBack(queryRefer);

            List<UrlItem> urlItemList = new ArrayList<>();


            while (cursorRefer.moveToNext()) {
                String desc1 = cursorRefer.getString(cursorRefer.getColumnIndex(DESC1_COLUMN));
                String refer = String.format(mUrl, cursorRefer.getString(cursorRefer.getColumnIndex(REFER_COLUMN)));

                UrlItem urlItem = new UrlItem(desc1, refer);

                urlItemList.add(urlItem);
            }

            cursorRefer.close();

            mDataBaseHelper.closeDB();
            return urlItemList;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
