package com.osagoandkasko.bestonline.repository;

import android.content.Context;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

public final class RepositoryProvider {

    private static InsuranceRepository sInsuranceRepository;

    private RepositoryProvider() {
    }

    @NonNull
    public static InsuranceRepository provideRepository(Context context) {
        if (sInsuranceRepository == null) {
            sInsuranceRepository = new DefaultInsuranceRepository(context.getApplicationContext());
        }
        return sInsuranceRepository;
    }

    @SuppressWarnings("unused")
    public static void setRepository(@NonNull InsuranceRepository insuranceRepository) {
        sInsuranceRepository = insuranceRepository;
    }

    @MainThread
    public static void init(Context context) {
        sInsuranceRepository = new DefaultInsuranceRepository(context.getApplicationContext());
    }
}
