package com.osagoandkasko.bestonline.content;

import java.io.Serializable;

public class Category implements Serializable {

    private int mId;
    private String mTitle;
    private String mDesc1;
    private String mDesc2;

    public Category(int id, String title, String desc1, String desc2) {
        mId = id;
        mTitle = title;
        mDesc1 = desc1;
        mDesc2 = desc2;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }


    public String getDesc1() {
        return mDesc1;
    }

    public String getDesc2() {
        return mDesc2;
    }
}
