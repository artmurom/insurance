package com.osagoandkasko.bestonline.content;

import java.io.Serializable;

public class Insurance implements Serializable {

    private String mTitle;
    private String mImage;
    private String mDescList1;
    private String mDescList2;
    private String mDescFull;
    private String mRefer;
    private String mPromo;
    private String mCategoryName;

    public Insurance(String title, String image, String descList1, String descList2, String descFull, String refer, String promo, String categoryName) {
        mTitle = title;
        mImage = image;
        mDescList1 = descList1;
        mDescList2 = descList2;
        mDescFull = descFull;
        mRefer = refer;
        mPromo = promo;
        mCategoryName = categoryName;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getImage() {
        return mImage;
    }

    public String getDescList1() {
        return mDescList1;
    }

    public String getDescList2() {
        return mDescList2;
    }

    public String getDescFull() {
        return mDescFull;
    }

    public String getRefer() {
        return mRefer;
    }

    public String getPromo() {
        return mPromo;
    }

    public String getCategoryName() {
        return mCategoryName;
    }
}
