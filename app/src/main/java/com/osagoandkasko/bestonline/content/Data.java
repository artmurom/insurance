package com.osagoandkasko.bestonline.content;

import java.io.Serializable;
import java.util.List;

public class Data implements Serializable {

    private List<Insurance> mInsuranceList;
    private Category mCategory;

    public Data(List<Insurance> insuranceList, Category category) {
        mInsuranceList = insuranceList;
        mCategory = category;
    }

    public List<Insurance> getInsuranceList() {
        return mInsuranceList;
    }

    public Category getCategory() {
        return mCategory;
    }
}
