package com.osagoandkasko.bestonline.content;

public class UrlItem {

    private final String mTitle;
    private final String mUrl;

    public UrlItem(String title, String url) {
        mTitle = title;
        mUrl = url;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getUrl() {
        return mUrl;
    }
}
