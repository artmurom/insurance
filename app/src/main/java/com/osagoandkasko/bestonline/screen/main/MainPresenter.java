package com.osagoandkasko.bestonline.screen.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.osagoandkasko.bestonline.repository.InsuranceRepository;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    MainPresenter(InsuranceRepository insuranceRepository) {

        getViewState().initUI(insuranceRepository.getData());
    }

}
