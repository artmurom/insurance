package com.osagoandkasko.bestonline.screen.insurance;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.osagoandkasko.bestonline.R;
import com.osagoandkasko.bestonline.content.Category;
import com.osagoandkasko.bestonline.content.Data;
import com.osagoandkasko.bestonline.content.Insurance;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


class InsuranceAdapter extends RecyclerView.Adapter<BaseHolder> {

    private static final int HEADER = 0;
    private static final int CONTENT = 1;

    private static final String TAG = "InsuranceAdapter";

    private final List<Insurance> mInsuranceList;
    private final Category mCategory;
    private final OnItemClick mOnItemClick;

    InsuranceAdapter(OnItemClick onItemClick, Data data) {
        mInsuranceList = data.getInsuranceList();
        mCategory = data.getCategory();
        mOnItemClick = onItemClick;
    }


    @NonNull
    @Override
    public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == HEADER) {
            return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false));
        } else {
            return new ContentHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_insurance, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseHolder holder, int position) {
        if (position == 0) {
            HeaderHolder headerHolder = (HeaderHolder) holder;
            headerHolder.bind(mCategory);
        } else {
            ContentHolder contentHolder = (ContentHolder) holder;
            Insurance insurance = mInsuranceList.get(position - 1);
            contentHolder.bind(insurance);
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        }
        return CONTENT;
    }

    @Override
    public int getItemCount() {
        return mInsuranceList.size() + 1;
    }

    class ContentHolder extends BaseHolder {

        @BindView(R.id.ivImage)
        ImageView mIvImage;

        @BindView(R.id.tvDesc)
        TextView mTvDesc;

        @BindView(R.id.tvSubDesc)
        TextView mTvSubDesc;

        @BindView(R.id.tvPromo)
        TextView mTvPromo;

        @BindView(R.id.btnMakeRequest)
        Button mBtnMakeRequest;


        ContentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Insurance insurance) {
            try {
                String imagePath = "image/" + insurance.getImage();
                InputStream ims = itemView.getContext().getAssets().open(imagePath);
                Drawable d = Drawable.createFromStream(ims, null);
                mIvImage.setImageDrawable(d);
                ims.close();
            } catch (IOException ex) {
                Log.e(TAG, " error load assets");
            }

            mTvDesc.setText(insurance.getDescList1());
            mTvSubDesc.setText(insurance.getDescList2());
            String promo = insurance.getPromo();
            if(promo==null) {
                mTvPromo.setVisibility(View.GONE);
            }  else {
                mTvPromo.setText(promo);
            }
            mBtnMakeRequest.setOnClickListener(v -> mOnItemClick.onButtonMakeRequestClick(insurance.getRefer()));

        }
    }

    class HeaderHolder extends BaseHolder {

        @BindView(R.id.tvDesc1)
        TextView mTvDesc1;

        @BindView(R.id.tvDesc2)
        TextView mTvDesc2;

        @BindView(R.id.btnInfo)
        Button mBtnInfo;


        HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Category category) {
            mTvDesc1.setText(category.getDesc1());
            mTvDesc2.setText(category.getDesc2());

            mBtnInfo.setOnClickListener(view -> mOnItemClick.onInfoClick());
        }
    }


    interface OnItemClick {

        void onButtonMakeRequestClick(String refer);

        void onInfoClick();
    }
}
