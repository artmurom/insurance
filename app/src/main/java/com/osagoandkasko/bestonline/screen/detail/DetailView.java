package com.osagoandkasko.bestonline.screen.detail;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.osagoandkasko.bestonline.content.Data;
import com.osagoandkasko.bestonline.content.Insurance;
import com.osagoandkasko.bestonline.content.UrlItem;

import java.util.List;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface DetailView extends MvpView {
    void initUI(Insurance insurance, List<UrlItem> urlItemList);
}
