package com.osagoandkasko.bestonline.screen.main;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.osagoandkasko.bestonline.R;
import com.osagoandkasko.bestonline.content.Data;
import com.osagoandkasko.bestonline.repository.RepositoryProvider;
import com.osagoandkasko.bestonline.screen.insurance.InsuranceFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    private static final String TAG = "MainActivity";

    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;

    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    @InjectPresenter
    MainPresenter mPresenter;

    @ProvidePresenter
    public MainPresenter createMainPresenter() {
        return new MainPresenter(RepositoryProvider.provideRepository(this));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

    }

    @Override
    public void initUI(List<Data> dataList) {
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), dataList);

        mViewPager.setAdapter(myPagerAdapter);

        mTabLayout.setupWithViewPager(mViewPager);
    }


    public static class MyPagerAdapter extends FragmentPagerAdapter {

        private static List<Data> mDataList;

        MyPagerAdapter(FragmentManager fm, List<Data> dataList) {
            super(fm);
            mDataList = dataList;
        }

        @Override
        public Fragment getItem(int position) {
            return InsuranceFragment.newInstance(mDataList.get(position));
        }

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mDataList.get(position).getCategory().getTitle();
        }

    }


}
