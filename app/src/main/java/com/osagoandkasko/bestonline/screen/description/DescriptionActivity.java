package com.osagoandkasko.bestonline.screen.description;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.osagoandkasko.bestonline.R;
import com.osagoandkasko.bestonline.content.Data;
import com.osagoandkasko.bestonline.content.Insurance;
import com.osagoandkasko.bestonline.screen.detail.DetailActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DescriptionActivity extends AppCompatActivity implements DescriptionAdapter.OnItemClick {

    private static final String EXTRA_DATA = "data";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;


    public static Intent makeIntent(AppCompatActivity activity, Data data) {
        Intent intent = new Intent(activity, DescriptionActivity.class);
        intent.putExtra(EXTRA_DATA, data);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        ButterKnife.bind(this);

        Data data = (Data) getIntent().getSerializableExtra(EXTRA_DATA);

        mToolbar.setNavigationIcon(R.drawable.ic_arrow);
        mToolbar.setNavigationOnClickListener(v -> onBackPressed());

        DescriptionAdapter descriptionAdapter = new DescriptionAdapter(this, data.getInsuranceList());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mRecyclerView.setAdapter(descriptionAdapter);

    }

    @Override
    public void onButtonClick(Insurance insurance) {
        startActivity(DetailActivity.makeIntent(this, insurance));
    }
}
