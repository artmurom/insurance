package com.osagoandkasko.bestonline.screen.detail;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.osagoandkasko.bestonline.content.Insurance;
import com.osagoandkasko.bestonline.content.UrlItem;
import com.osagoandkasko.bestonline.repository.InsuranceRepository;

import java.util.List;

@InjectViewState
public class DetailPresenter extends MvpPresenter<DetailView> {

    private InsuranceRepository mRepository;


    DetailPresenter(InsuranceRepository insuranceRepository) {
        mRepository = insuranceRepository;
    }

    public void init(Insurance insurance) {
        List<UrlItem> urlItemList = mRepository.getAllUrl(insurance.getTitle(), insurance.getCategoryName());

        getViewState().initUI(insurance, urlItemList);
    }


}
