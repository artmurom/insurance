package com.osagoandkasko.bestonline.screen.insurance;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.osagoandkasko.bestonline.R;
import com.osagoandkasko.bestonline.content.Data;
import com.osagoandkasko.bestonline.content.Insurance;
import com.osagoandkasko.bestonline.screen.description.DescriptionActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class InsuranceFragment extends Fragment implements InsuranceAdapter.OnItemClick {


    private static final String EXTRA_DATA = "data";

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private Context mContext;

    private Data mData;

    public InsuranceFragment() {
    }

    public static InsuranceFragment newInstance(Data data) {
        InsuranceFragment fragment = new InsuranceFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mData = (Data) getArguments().getSerializable(EXTRA_DATA);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_insurance, container, false);

        ButterKnife.bind(this, rootView);


        InsuranceAdapter insuranceAdapter = new InsuranceAdapter(this, mData);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        mRecyclerView.setAdapter(insuranceAdapter);

        return rootView;
    }

    @Override
    public void onButtonMakeRequestClick(String refer) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(refer));
        startActivity(browserIntent);
    }

    @Override
    public void onInfoClick() {
        List<Insurance> insuranceList = mData.getInsuranceList();
        List<Insurance> filteredInsuranceList = new ArrayList<>();
        filteredInsuranceList.add(insuranceList.get(0));

        for (Insurance insurance : insuranceList) {
            boolean isAlreadyContains = false;
            for (Insurance filteredInsurance : filteredInsuranceList) {
                if (filteredInsurance.getTitle().equals(insurance.getTitle())) {
                    isAlreadyContains = true;
                }
            }
            if (!isAlreadyContains) {
                filteredInsuranceList.add(insurance);
            }
        }

        Data data = new Data(filteredInsuranceList, mData.getCategory());

        startActivity(DescriptionActivity.makeIntent((AppCompatActivity) mContext, data));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }
}
