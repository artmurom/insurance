package com.osagoandkasko.bestonline.screen.detail;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.osagoandkasko.bestonline.R;
import com.osagoandkasko.bestonline.content.Insurance;
import com.osagoandkasko.bestonline.content.UrlItem;
import com.osagoandkasko.bestonline.repository.RepositoryProvider;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailActivity extends MvpAppCompatActivity implements DetailView {

    private static final String TAG = "DetailActivity";

    private static final String EXTRA_INSURANCE = "insurance";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tvTitle)
    TextView mTvTitle;

    @BindView(R.id.ivImage)
    ImageView mIvImage;

    @BindView(R.id.tvDesc)
    TextView mTvDesc;

    @BindView(R.id.btnMakeRequest)
    Button mBtnMakeRequest;


    @InjectPresenter
    DetailPresenter mPresenter;

    @ProvidePresenter
    public DetailPresenter createDetailPresenter() {
        return new DetailPresenter(RepositoryProvider.provideRepository(this));
    }

    public static Intent makeIntent(AppCompatActivity activity, Insurance insurance) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(EXTRA_INSURANCE, insurance);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);


        mToolbar.setNavigationIcon(R.drawable.ic_arrow);
        mToolbar.setNavigationOnClickListener(v -> onBackPressed());

        Insurance insurance = (Insurance) getIntent().getSerializableExtra(EXTRA_INSURANCE);

        mPresenter.init(insurance);
    }

    @Override
    public void initUI(Insurance insurance, List<UrlItem> urlItemList) {
        try {
            String imagePath = "image/" + insurance.getImage();
            InputStream ims = getAssets().open(imagePath);
            Drawable d = Drawable.createFromStream(ims, null);
            mIvImage.setImageDrawable(d);
            ims.close();
        } catch (IOException ex) {
            Log.e(TAG, " error load assets");
        }

        mTvTitle.setText(insurance.getTitle());
        mTvDesc.setText(Html.fromHtml(insurance.getDescFull()));
        mBtnMakeRequest.setOnClickListener(v -> {
            if (urlItemList.size() == 1) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(insurance.getRefer()));
                startActivity(intent);
            } else {
                String[] titleArray = new String[urlItemList.size()];
                String[] referArray = new String[urlItemList.size()];

                for (int i = 0; i < urlItemList.size(); i++) {
                    UrlItem urlItem = urlItemList.get(i);
                    titleArray[i] = urlItem.getTitle();
                    referArray[i] = urlItem.getUrl();
                }

                new MaterialDialog.Builder(this)
                        .title(insurance.getTitle())
                        .items(titleArray)
                        .itemsCallback((dialog, view, which, text) -> {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(referArray[which]));
                            startActivity(intent);
                        })
                        .show();
            }
        });


    }
}
