package com.osagoandkasko.bestonline.screen.insurance;

import android.support.v7.widget.RecyclerView;
import android.view.View;

class BaseHolder extends RecyclerView.ViewHolder {
    BaseHolder(View itemView) {
        super(itemView);
    }
}
