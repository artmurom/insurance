package com.osagoandkasko.bestonline.screen.description;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.osagoandkasko.bestonline.R;
import com.osagoandkasko.bestonline.content.Insurance;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


class DescriptionAdapter extends RecyclerView.Adapter<DescriptionAdapter.DescriptionHolder> {

    private static final String TAG = "DescriptionAdapter";

    private final List<Insurance> mItemList;
    private final OnItemClick mOnItemClick;

    DescriptionAdapter(OnItemClick onItemClick, List<Insurance> itemList) {
        mItemList = itemList;
        mOnItemClick = onItemClick;
    }

    @NonNull
    @Override
    public DescriptionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_description, parent, false);
        return new DescriptionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DescriptionHolder holder, int position) {
        Insurance item = mItemList.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    class DescriptionHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivImage)
        ImageView mIvLogo;

        @BindView(R.id.btnRead)
        Button mBtnRead;


        DescriptionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Insurance insurance) {

            try {
                String imagePath = "image/" + insurance.getImage();
                InputStream ims = itemView.getContext().getAssets().open(imagePath);
                Drawable d = Drawable.createFromStream(ims, null);
                mIvLogo.setImageDrawable(d);
                ims.close();
            } catch (IOException ex) {
                Log.e(TAG, " error load assets");
            }

            mBtnRead.setTag(insurance);
            mBtnRead.setOnClickListener(v -> mOnItemClick.onButtonClick(insurance));
        }
    }

    interface OnItemClick {

        void onButtonClick(Insurance insurance);

    }
}
