package com.osagoandkasko.bestonline.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "insurance.db";

    private static final String VERSION = "1";

    private SQLiteDatabase myDataBase;
    private final Context mContext;

    private Cursor cursor;

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.mContext = context;
    }

    private void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if (!dbExist) {
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase(){
        File dbFile = mContext.getDatabasePath(DB_NAME);

        return dbFile.exists();
    }

    private void copyDataBase() throws IOException{
        InputStream myInput = mContext.getAssets().open(DB_NAME);

        String outFileName =  mContext.getDatabasePath(DB_NAME).getPath();

        OutputStream myOutput = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        String myPath =  mContext.getDatabasePath(DB_NAME).getPath();
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public void close() {
        if(myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    public void upgradeDB()  {
        try {
            createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            openDataBase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "SELECT version FROM version WHERE _id = \"1\"";
        cursor = myDataBase.rawQuery(query, null);
        cursor.moveToFirst();
        String versionDB = cursor.getString(cursor.getColumnIndex("version"));

        if(!versionDB.equals(VERSION)) {
            try {
                this.getReadableDatabase();
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
        cursor.close();
        myDataBase.close();

    }


    public Cursor cursorBack(String query) {
        cursor = myDataBase.rawQuery(query, null);
        return cursor;
    }

    public void closeDB() {
        myDataBase.close();
    }
}